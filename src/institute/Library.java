/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package institute;

/**
 *
 * @author Admin
 */
public class Library {
   
    private String name;
    private int groupofbooks;
    
    public Library(String name, int groupofbooks) {
        this.name = name;
        this.groupofbooks = groupofbooks;
    }
    
    public String getName() {
        return this.name;
    }
    
     public int getGroupofbooks() {
        return this.groupofbooks;
    }
}
